package spaceBattleServer.actors;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;

import gameServer.CollisionMap;
import gameServer.MultiplayerActorModel;
import gameServer.WorldModel;
import spaceBattleClient.actors.ClientLaser;

public class ServerLaser extends MultiplayerActorModel {
	
	private double speed;
	private int ttl;
	private String ownerId;

	public ServerLaser(String actorId, int duration, double speed) throws IOException {
		super(actorId);
		InputStream stream = CollisionMap.class.getClassLoader().getResourceAsStream("laserGreen04.png");
		BufferedImage img = ImageIO.read(stream);
		setImage(img);
		this.speed = speed;
		this.ttl = duration;
		setClientClass(ClientLaser.class);
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String owner) {
		this.ownerId = owner;
	}
	
	private void edgeLoop() {
		WorldModel world = getWorld();
		if (getX() > world.getWidth() - getWidth() / 2) {
			setX(-getWidth() / 2);
		} else if (getX() < -getWidth() / 2) {
			setX(world.getWidth() - getWidth() / 2);
		}
		
		if (getY() > world.getHeight() - getHeight() / 2) {
			setY(-getHeight() / 2);
		} else if (getY() < -getHeight() / 2) {
			setY(world.getHeight() - getHeight() / 2);
		}
	}

	@Override
	public void act() {
		double dx = speed * Math.cos(getRotation());
		double dy = speed * Math.sin(getRotation());
		move(dx, dy);
		edgeLoop();
		ttl--;
		if (ttl == 0) {
			getWorld().remove(this);
		} else {
			List<ServerPlayer> players = getIntersectingActors(ServerPlayer.class);
			for (ServerPlayer player : players) {
				if (!player.getActorId().equals(getOwnerId())) {
					player.onHitByLaser();
					getWorld().remove(this);
					break;
				}
			} 
		}
	}
}
