package spaceBattleServer;


import java.io.IOException;
import java.net.ServerSocket;

import gameServer.MultiThreadServer;
import spaceBattleServer.world.SpaceBattleServerWorld;

public class ServerDriver {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		int port = 1234;
		if (args.length > 0) {
			try {
				port = Integer.parseInt(args[0]);
			} catch (NumberFormatException err) {
				System.err.println("port must be an integer");
				err.printStackTrace();
			}
		}
		ServerSocket ssock = new ServerSocket(port);
		MultiThreadServer serve = new MultiThreadServer(ssock);
		SpaceBattleServerWorld world = new SpaceBattleServerWorld(1000, 700, serve);
		world.start();
		new Thread(serve).start();
	}

}
