package spaceBattleServer.commands;

import command.ServerCommandHandler;
import gameServer.ServerWorld;
import spaceBattleServer.actors.ServerPlayer;

public class RotateRightCommand extends ServerCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, ServerWorld world) {
		if (params.length < 1) throw new IllegalArgumentException("Not enough parameters. Correct params are <id>");
		String id = params[0];
		ServerPlayer player = (ServerPlayer)world.getMultiplayerActors().get(id);
		if (player != null) {
			player.setRotation(player.getRotation() + 0.05);
		}

	}

	@Override
	public String getCommandWord() {
		return "RR";
	}

}
