package spaceBattleServer.commands;

import java.io.IOException;
import java.net.URISyntaxException;

import command.ServerCommandHandler;
import gameServer.ServerWorld;
import spaceBattleServer.world.SpaceBattleServerWorld;

public class RestartCommand extends ServerCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, ServerWorld world) {
		if (params.length < 1) throw new IllegalArgumentException("Not enough parameters. Correct params are <id>");
		String id = params[0];
		try {
			SpaceBattleServerWorld sbw = (SpaceBattleServerWorld) world;
			sbw.addPlayer(id);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getCommandWord() {
		return "RESTART";
	}

}
