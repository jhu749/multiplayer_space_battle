package spaceBattleServer.commands;

import command.ServerCommandHandler;
import gameServer.ServerWorld;
import spaceBattleServer.actors.ServerPlayer;

public class UpCommand extends ServerCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, ServerWorld world) {
		if (params.length < 1) throw new IllegalArgumentException("Not enough parameters. Correct params are <id>");
		String id = params[0];
		ServerPlayer player = (ServerPlayer)world.getMultiplayerActors().get(id);
		if (player != null) {
			if (player.getDy() > -10) player.setDy(player.getDy() - 0.1);
		}

	}

	@Override
	public String getCommandWord() {
		return "U";
	}

}
