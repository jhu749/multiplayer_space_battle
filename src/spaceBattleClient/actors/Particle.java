package spaceBattleClient.actors;


import graphicsEngine.Actor;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class Particle extends Actor {
	
	private double dx;
	private double dy;
	private double ddx;
	private double ddy;
	private double dOpacity;
	private double ddOpacity;
	private long timeAlive;
	private long timeToLive;
	private boolean active;
	
	public Particle() {
		reset();
	}
	
	public void reset() {
		dx = 0;
		dy = 0;
		ddx = 0;
		ddy = 0;
		dOpacity = 0;
		ddOpacity = 0;
		timeAlive = 0;
		timeToLive = 100;
		active = false;
		setZOrder(2);
	}
	
	public void fillWithColor(Color color) {
		WritableImage img = new WritableImage(1, 1);
		img.getPixelWriter().setColor(0, 0, color);
		setImage(img);
	}
	
	@Override
	public void act(long now) {
		if (active) {
			timeAlive++;
			if (timeAlive > timeToLive) {
				getWorld().remove(this);
			} else {
				dx += ddx;
				dy += ddy;
				move(dx, dy);
				dOpacity += ddOpacity;
				setOpacity(getOpacity() + dOpacity);
			}
		}
	}

	public double getDx() {
		return dx;
	}

	public void setDx(double dx) {
		this.dx = dx;
	}

	public double getDy() {
		return dy;
	}

	public void setDy(double dy) {
		this.dy = dy;
	}

	public double getDdx() {
		return ddx;
	}

	public void setDdx(double ddx) {
		this.ddx = ddx;
	}

	public double getDdy() {
		return ddy;
	}

	public void setDdy(double ddy) {
		this.ddy = ddy;
	}

	public double getdOpacity() {
		return dOpacity;
	}

	public void setdOpacity(double dOpacity) {
		this.dOpacity = dOpacity;
	}

	public double getDdOpacity() {
		return ddOpacity;
	}

	public void setDdOpacity(double ddOpacity) {
		this.ddOpacity = ddOpacity;
	}

	public long getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}

	public long getTimeAlive() {
		return timeAlive;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}

